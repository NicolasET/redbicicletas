var bicicleta = require('../../models/bicicleta');

exports.bicicletaList = function(req, res){
    res.status(200).json({
        bicicletas: bicicleta.allBicis
    });
}

exports.bicicletaCreate = function(req, res){
    var bici = new bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicletaDelete = function(req, res){
    bicicleta.removeById(req.body.id);

    res.status(204).send();
}