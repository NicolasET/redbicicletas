var bicicleta = require('../models/bicicleta');

exports.bicicletaList = function(req, res){
    res.render('bicicletas/index', {bicis: bicicleta.allBicis})
}

exports.bicicletaCreateGET = function(req, res){
    res.render('bicicletas/create');
}

exports.bicicletaCreatePOST = function(req, res){
    var bici = new bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    bicicleta.add(bici);

    res.redirect('/bicicletas');
}

exports.bicicletaUpdateGET = function(req, res){
    var bici = bicicleta.findById(req.params.id)
    res.render('bicicletas/update', {bici});
}

exports.bicicletaUpdatePOST = function(req, res){
    var bici = bicicleta.findById(req.params.id)
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.redirect('/bicicletas');
}

exports.bicicletaDeletePOST = function(req, res){
    bicicleta.removeById(req.body.id);

    res.redirect('/bicicletas');
}