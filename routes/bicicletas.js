var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicleta');

router.get('/', bicicletaController.bicicletaList);
router.get('/create', bicicletaController.bicicletaCreateGET);
router.post('/create', bicicletaController.bicicletaCreatePOST);
router.get('/:id/update', bicicletaController.bicicletaUpdateGET);
router.post('/:id/update', bicicletaController.bicicletaUpdatePOST);
router.post('/:id/delete', bicicletaController.bicicletaDeletePOST);

module.exports = router;