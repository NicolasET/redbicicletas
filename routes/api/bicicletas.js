var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaController.bicicletaList);
router.post('/create', bicicletaController.bicicletaCreate);
router.delete('/delete', bicicletaController.bicicletaDelete);

module.exports = router;